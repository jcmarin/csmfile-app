import React, { Component } from 'react'

class LeftMenu  extends Component {
    
    render() {
        console.log(this.props);
        const { menuItems } = this.props;
        return (
           <div>
            <ul>
             {menuItems.map(menuItem => ( 
                  <div key={menuItem.key}>{menuItem.label}</div>
                ))}
            </ul>
            </div>
          );
    }
}

export default LeftMenu
