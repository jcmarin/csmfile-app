import React, {Component} from 'react';
import Navbar from './components/Navbar';
import LeftMenu from './components/leftMenu';
import Container from './components/Container';
import './App.css';

 class App extends Component {
  state = {
    count: 0,
    menus: [{key: "1", label: "First"},
    {key: "2", label: "Second"},
    {key: "3", label: "Third"},
    {key: "4", label: "Next"},
    {key: "5", label: "Last"}
  
  ],
    isOn: false
  }
  render() {
  return (
    <div className="App">
      <div>
      <Navbar/>
      </div> 
      <div className="Container">
      <div className="LeftMenu">
         <LeftMenu menuItems={this.state.menus}/>
      </div><div className="ContentBody">
        <Container/>
      </div>
        
      </div>
     
      
    </div>
  );
  }
}

export default App;
